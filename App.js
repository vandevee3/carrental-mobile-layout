/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Button,
  TouchableOpacity,
  Image
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import HomeScreen from './screen/HomeScreen';
import Icon from 'react-native-vector-icons/AntDesign';
import VectorImage from 'react-native-vector-image';

import CarList from './screen/CarList';
import Account from './screen/Account';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import SplashScreen from 'react-native-splash-screen'


const Stack = createNativeStackNavigator();

function HomeScreenView(props){
  React.useEffect(() => {
    SplashScreen.hide();
  });

  return(
    <View style ={styles.mainContainer}>
      <HomeScreen />
      <View style={styles.menuContainer}>
          <View style={styles.homeMenuContainer}>
            <TouchableOpacity style={styles.homeButton}>
              <Image style={{width : 24, height : 24}} source={require('./asset/icon/homepickedIcon.png')}/>
              <Text style ={{color : '#0D28A6'}}>Home</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.carListMenuContainer}>
            <TouchableOpacity style={styles.carButton} onPress={ () => props.navigation.navigate('Carlist')}>
              <Image style={{width : 24, height : 24}} source={require('./asset/icon/listIcon.png')}/>
              <Text style ={styles.carText}>Daftar Mobil</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.accountMenuContainer}>
            <TouchableOpacity style={styles.accountButton} onPress={() => props.navigation.navigate('Account')}>
              <Image style={{width : 24, height : 24}} source={require('./asset/icon/accountIcon.png')}/>
              <Text style ={styles.accountText}>Akun</Text>
            </TouchableOpacity>
          </View>
        </View>
    </View>
  );
}

function CarListView (props){
  return(
   <View style={styles.mainContainer}>
    <CarList/>
    <View style={styles.menuContainer}>
          <View style={styles.homeMenuContainer}>
            <TouchableOpacity style={styles.homeButton} onPress={() => props.navigation.navigate("Home")}>
              <Image style={{width : 24, height : 24}} source={require('./asset/icon/homeIcon.png')}/>
              <Text style ={styles.homeText}>Home</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.carListMenuContainer}>
            <TouchableOpacity style={styles.carButton}>
              <Image style={{width : 24, height : 24}} source={require('./asset/icon/listpickedIcon.png')}/>
              <Text style ={{color : "#0D28A6"}}>Daftar Mobil</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.accountMenuContainer}>
            <TouchableOpacity style={styles.accountButton} onPress={() => props.navigation.navigate('Account')}>
              <Image style={{width : 24, height : 24}} source={require('./asset/icon/accountIcon.png')}/>
              <Text style ={styles.accountText}>Akun</Text>
            </TouchableOpacity>
          </View>
      </View> 
   </View>
  );
}

function AccountView(props){
  return(
    <View style={styles.mainContainer}>
    <Account/>
    <View style={styles.menuContainer}>
          <View style={styles.homeMenuContainer}>
            <TouchableOpacity style={styles.homeButton} onPress={() => props.navigation.navigate("Home")}>
              <Image style={{width : 24, height : 24}} source={require('./asset/icon/homeIcon.png')}/>
              <Text style ={styles.homeText}>Home</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.carListMenuContainer}>
            <TouchableOpacity style={styles.carButton} onPress={() => props.navigation.navigate("Carlist")}>
              <Image style={{width : 24, height : 24}} source={require('./asset/icon/listIcon.png')}/>
              <Text style ={styles.carText}>Daftar Mobil</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.accountMenuContainer}>
            <TouchableOpacity style={styles.accountButton}>
              <Image style={{width : 24, height : 24}} source={require('./asset/icon/accountpickedIcon.png')}/>
              <Text style ={{color : "#0D28A6"}}>Akun</Text>
            </TouchableOpacity>
          </View>
      </View> 
  </View>
  );
}

const App: () => Node = () => {
  return (
    <NavigationContainer>
      
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreenView} options={{headerShown: false}}/>
        <Stack.Screen name="Carlist" component={CarListView} options={{headerShown: false}}/>
        <Stack.Screen name="Account" component={AccountView} options={{headerShown: false}}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const styles = StyleSheet.create({ 
  mainContainer : {
    flex : 1, 
    height : '100%',
    backgroundColor : 'white'
  },
  menuContainer : {
    justifyContent: 'space-around',
    alignContent : 'flex-end',
    borderColor : 'black',
    borderWidth : 0.3,
    height : '10%',
    flexDirection : 'row',
    backgroundColor : 'white'
  },
  homeMenuContainer : {
    justifyContent : 'center'
  },
  carListMenuContainer : {
    justifyContent : 'center'
  },
  accountMenuContainer : {
    justifyContent : 'center'
  },  
  homeButton : {
    alignItems  : 'center',
    justifyContent : 'center'
  },
  carButton :{
    alignItems  : 'center',
    justifyContent : 'center'
  },
  accountButton : {
    alignItems : 'center',
    justifyContent : 'center'
  }
});

export default App;
