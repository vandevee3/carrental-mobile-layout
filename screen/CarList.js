import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Button,
  TouchableOpacity,
  Image
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

export default function CarList (){
    return(
       <View style={{height : '90%'}}>
        <Text style={styles.daftarMobilText}>Daftar Mobil Pilihan</Text>
        <ScrollView style={styles.scrollHomeView}>
            <View style={styles.carListContainer}>
              <Image 
                style={styles.carListImage} 
                source = {require('../asset/mobil.png')}
              />
              <View style={styles.carListInfo}>
                <Text style={styles.carListText}>Daihatsu Xenia</Text>
                <View style={{flexDirection :'row', marginTop : 5}}>
                  <View style={{flexDirection : 'row'}}>
                    <Image style={{width : 12, height : 12}} source ={require('../asset/icon/personIcon.png')}/>
                    <Text style={{fontFamily : 'Helvetica',fontStyle : 'normal',fontWeight : '300',fontSize : 10,lineHeight : 14,color : '#8A8A8A', marginLeft : 4.5}}>4</Text>
                  </View>
                  <View style={{flexDirection : 'row', marginLeft : 17}}>
                    <Image style={{width : 12, height : 12}} source ={require('../asset/icon/tasIcon.png')}/>
                    <Text style={{fontFamily : 'Helvetica',fontStyle : 'normal',fontWeight : '300',fontSize : 10,lineHeight : 14,color : '#8A8A8A', marginLeft : 4.5}}>2</Text>
                  </View>
                </View>
                <Text style={styles.priceRentCar}>Rp 230.000</Text>
              </View>
            </View>
            <View style={styles.carListContainer}>
              <Image 
                style={styles.carListImage} 
                source = {require('../asset/mobil.png')}
              />
              <View style={styles.carListInfo}>
                <Text style={styles.carListText}>Daihatsu Xenia</Text>
                <View style={{flexDirection :'row', marginTop : 5}}>
                  <View style={{flexDirection : 'row'}}>
                    <Image style={{width : 12, height : 12}} source ={require('../asset/icon/personIcon.png')}/>
                    <Text style={{fontFamily : 'Helvetica',fontStyle : 'normal',fontWeight : '300',fontSize : 10,lineHeight : 14,color : '#8A8A8A', marginLeft : 4.5}}>4</Text>
                  </View>
                  <View style={{flexDirection : 'row', marginLeft : 17}}>
                    <Image style={{width : 12, height : 12}} source ={require('../asset/icon/tasIcon.png')}/>
                    <Text style={{fontFamily : 'Helvetica',fontStyle : 'normal',fontWeight : '300',fontSize : 10,lineHeight : 14,color : '#8A8A8A', marginLeft : 4.5}}>2</Text>
                  </View>
                </View>
                <Text style={styles.priceRentCar}>Rp 230.000</Text>
              </View>
            </View>
            <View style={styles.carListContainer}>
              <Image 
                style={styles.carListImage} 
                source = {require('../asset/mobil.png')}
              />
              <View style={styles.carListInfo}>
                <Text style={styles.carListText}>Daihatsu Xenia</Text>
                <View style={{flexDirection :'row', marginTop : 5}}>
                  <View style={{flexDirection : 'row'}}>
                    <Image style={{width : 12, height : 12}} source ={require('../asset/icon/personIcon.png')}/>
                    <Text style={{fontFamily : 'Helvetica',fontStyle : 'normal',fontWeight : '300',fontSize : 10,lineHeight : 14,color : '#8A8A8A', marginLeft : 4.5}}>4</Text>
                  </View>
                  <View style={{flexDirection : 'row', marginLeft : 17}}>
                    <Image style={{width : 12, height : 12}} source ={require('../asset/icon/tasIcon.png')}/>
                    <Text style={{fontFamily : 'Helvetica',fontStyle : 'normal',fontWeight : '300',fontSize : 10,lineHeight : 14,color : '#8A8A8A', marginLeft : 4.5}}>2</Text>
                  </View>
                </View>
                <Text style={styles.priceRentCar}>Rp 230.000</Text>
              </View>
            </View>
            <View style={styles.carListContainer}>
              <Image 
                style={styles.carListImage} 
                source = {require('../asset/mobil.png')}
              />
              <View style={styles.carListInfo}>
                <Text style={styles.carListText}>Daihatsu Xenia</Text>
                <View style={{flexDirection :'row', marginTop : 5}}>
                  <View style={{flexDirection : 'row'}}>
                    <Image style={{width : 12, height : 12}} source ={require('../asset/icon/personIcon.png')}/>
                    <Text style={{fontFamily : 'Helvetica',fontStyle : 'normal',fontWeight : '300',fontSize : 10,lineHeight : 14,color : '#8A8A8A', marginLeft : 4.5}}>4</Text>
                  </View>
                  <View style={{flexDirection : 'row', marginLeft : 17}}>
                    <Image style={{width : 12, height : 12}} source ={require('../asset/icon/tasIcon.png')}/>
                    <Text style={{fontFamily : 'Helvetica',fontStyle : 'normal',fontWeight : '300',fontSize : 10,lineHeight : 14,color : '#8A8A8A', marginLeft : 4.5}}>2</Text>
                  </View>
                </View>
                <Text style={styles.priceRentCar}>Rp 230.000</Text>
              </View>
            </View>
            <View style={styles.carListContainer}>
              <Image 
                style={styles.carListImage} 
                source = {require('../asset/mobil.png')}
              />
              <View style={styles.carListInfo}>
                <Text style={styles.carListText}>Daihatsu Xenia</Text>
                <View style={{flexDirection :'row', marginTop : 5}}>
                  <View style={{flexDirection : 'row'}}>
                    <Image style={{width : 12, height : 12}} source ={require('../asset/icon/personIcon.png')}/>
                    <Text style={{fontFamily : 'Helvetica',fontStyle : 'normal',fontWeight : '300',fontSize : 10,lineHeight : 14,color : '#8A8A8A', marginLeft : 4.5}}>4</Text>
                  </View>
                  <View style={{flexDirection : 'row', marginLeft : 17}}>
                    <Image style={{width : 12, height : 12}} source ={require('../asset/icon/tasIcon.png')}/>
                    <Text style={{fontFamily : 'Helvetica',fontStyle : 'normal',fontWeight : '300',fontSize : 10,lineHeight : 14,color : '#8A8A8A', marginLeft : 4.5}}>2</Text>
                  </View>
                </View>
                <Text style={styles.priceRentCar}>Rp 230.000</Text>
              </View>
            </View>
            <View style={styles.carListContainer}>
              <Image 
                style={styles.carListImage} 
                source = {require('../asset/mobil.png')}
              />
              <View style={styles.carListInfo}>
                <Text style={styles.carListText}>Daihatsu Xenia</Text>
                <View style={{flexDirection :'row', marginTop : 5}}>
                  <View style={{flexDirection : 'row'}}>
                    <Image style={{width : 12, height : 12}} source ={require('../asset/icon/personIcon.png')}/>
                    <Text style={{fontFamily : 'Helvetica',fontStyle : 'normal',fontWeight : '300',fontSize : 10,lineHeight : 14,color : '#8A8A8A', marginLeft : 4.5}}>4</Text>
                  </View>
                  <View style={{flexDirection : 'row', marginLeft : 17}}>
                    <Image style={{width : 12, height : 12}} source ={require('../asset/icon/tasIcon.png')}/>
                    <Text style={{fontFamily : 'Helvetica',fontStyle : 'normal',fontWeight : '300',fontSize : 10,lineHeight : 14,color : '#8A8A8A', marginLeft : 4.5}}>2</Text>
                  </View>
                </View>
                <Text style={styles.priceRentCar}>Rp 230.000</Text>
              </View>
            </View>
        </ScrollView>
       </View>
    );
}


const styles = StyleSheet.create({
    scrollHomeView : {
        paddingTop : 20,
        marginLeft : 16,
        marginRight : 16,
        paddingBottom : 100,
        marginTop : 10
    },
    carListContainer :{
        flexDirection : 'row',
        paddingTop : 24,  
        paddingLeft : 24,
        shadowColor: 'green',
        borderWidth : 0.1,
        borderColor : 'white',
        borderTopRightRadius : 10,
        borderBottomRightRadius : 10,
        borderTopLeftRadius : 10,
        borderBottomLeftRadius :10,
        shadowOffset: {width: -2, height: 2},
        shadowOpacity: 0.1,
        // add shadows for Android only
        // No options for shadow color, shadow offset, shadow opacity like iOS
        elevation: 3,
        width : '100%',
        height : 98,
        marginBottom : 20
      },
      carListImage : {
        width : 40,
        height : 24
      },
      carListInfo : {
        marginLeft : 16
      },
      carListText :{
        fontFamily : 'Helvetica',
        fontStyle : 'normal',
        fontWeight : 'bold',
        fontSize : 14,
        lineHeight : 20,
        color : 'black'
      },
      priceRentCar : {
        fontFamily : 'Helvetica',
        fontStyle : 'normal',
        fontWeight : 'normal',
        fontSize : 14,
        lineHeight : 20,
        color : '#5CB85F'
      },
      daftarMobilText : {
        marginTop : 24,
        marginLeft : 16,
        color : 'black',
        fontFamily : 'Helvetica',
        fontStyle : 'normal',
        fontWeight : 'bold',
        fontSize : 14,
        lineHeight : 20,
      },
});