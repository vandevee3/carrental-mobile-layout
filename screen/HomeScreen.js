import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Button,
  TouchableOpacity,
  Image
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';


export default function HomeScreen (props){
    return (
      <View style={styles.mainContainer}>
        <View style={styles.infoContainer}>
          <View style ={styles.mainInfo}>
            <View style={styles.textInfo}>
              <Text style={styles.nameInfo}>Hi, Nama</Text>
              <Text style={styles.locationInfo}>Your Location</Text>
            </View>
            <Image style={styles.photoProfileStyle}
              source={require('../asset/icon/photoProfile.png')}
            />
          </View>
          <View style={styles.advertisementContainer}>
            <View>
            <Text style={styles.advertisementText}>Sewa Mobil Berkualitas{'\n'} di kawasanmu</Text>
            <TouchableOpacity style={styles.sewaButton}>
                <Text style={styles.sewaText}>Sewa Mobil</Text>
            </TouchableOpacity>
            </View>
            <Image style={styles.carImage} resizeMode = "cover"
                source = {require('../asset/imgcar.png')}
            />
          </View>
        </View>
        <View style={styles.menuHomeView}>
          <View View style={styles.homeMenuList}>
                <View style={styles.menuSewaMobil}>
                    <TouchableOpacity>
                        <Image style={styles.iconTruck} source={require('../asset/icon/icon_truck.png')}/>
                    </TouchableOpacity>
                    <Text style={styles.textMenuHome}>Sewa Mobil</Text>
                </View>
                <View style={styles.menuSewaMobil}>
                    <TouchableOpacity>
                        <Image style={styles.iconTruck} source={require('../asset/icon/oleh_oleh.png')}/>
                    </TouchableOpacity>
                    <Text style={styles.textMenuHome}>Oleh - Oleh</Text>
                </View>
                <View style={styles.menuSewaMobil}>
                    <TouchableOpacity>
                        <Image style={styles.iconTruck} source={require('../asset/icon/penginapan.png')}/>
                    </TouchableOpacity>
                    <Text style={styles.textMenuHome}>Penginapan</Text>
                </View>
                <View style={styles.menuSewaMobil}>
                    <TouchableOpacity>
                        <Image style={styles.iconTruck} source={require('../asset/icon/liburan.png')}/>
                    </TouchableOpacity>
                    <Text style={styles.textMenuHome}>Wisata</Text>
                </View>
            </View>
        </View>
        <Text style={styles.daftarMobilText}>Daftar Mobil Pilihan</Text>
        <ScrollView style={styles.scrollHomeView}>
            <View style={styles.carListContainer}>
              <Image 
                style={styles.carListImage} 
                source = {require('../asset/mobil.png')}
              />
              <View style={styles.carListInfo}>
                <Text style={styles.carListText}>Daihatsu Xenia</Text>
                <View style={{flexDirection :'row', marginTop : 5}}>
                  <View style={{flexDirection : 'row'}}>
                    <Image style={{width : 12, height : 12}} source ={require('../asset/icon/personIcon.png')}/>
                    <Text style={{fontFamily : 'Helvetica',fontStyle : 'normal',fontWeight : '300',fontSize : 10,lineHeight : 14,color : '#8A8A8A', marginLeft : 4.5}}>4</Text>
                  </View>
                  <View style={{flexDirection : 'row', marginLeft : 17}}>
                    <Image style={{width : 12, height : 12}} source ={require('../asset/icon/tasIcon.png')}/>
                    <Text style={{fontFamily : 'Helvetica',fontStyle : 'normal',fontWeight : '300',fontSize : 10,lineHeight : 14,color : '#8A8A8A', marginLeft : 4.5}}>2</Text>
                  </View>
                </View>
                <Text style={styles.priceRentCar}>Rp 230.000</Text>
              </View>
            </View>
            <View style={styles.carListContainer}>
              <Image 
                style={styles.carListImage} 
                source = {require('../asset/mobil.png')}
              />
              <View style={styles.carListInfo}>
                <Text style={styles.carListText}>Daihatsu Xenia</Text>
                <View style={{flexDirection :'row', marginTop : 5}}>
                  <View style={{flexDirection : 'row'}}>
                    <Image style={{width : 12, height : 12}} source ={require('../asset/icon/personIcon.png')}/>
                    <Text style={{fontFamily : 'Helvetica',fontStyle : 'normal',fontWeight : '300',fontSize : 10,lineHeight : 14,color : '#8A8A8A', marginLeft : 4.5}}>4</Text>
                  </View>
                  <View style={{flexDirection : 'row', marginLeft : 17}}>
                    <Image style={{width : 12, height : 12}} source ={require('../asset/icon/tasIcon.png')}/>
                    <Text style={{fontFamily : 'Helvetica',fontStyle : 'normal',fontWeight : '300',fontSize : 10,lineHeight : 14,color : '#8A8A8A', marginLeft : 4.5}}>2</Text>
                  </View>
                </View>
                <Text style={styles.priceRentCar}>Rp 230.000</Text>
              </View>
            </View>
        </ScrollView>
      </View>
    );
};


const styles = StyleSheet.create({ 
    mainContainer : {
      flex : 1, 
      height : '100%'
    },
    infoContainer : {
      backgroundColor : '#D3D9FD',
      height : '30%',
      paddingLeft : 16,
      paddingRight : 16,
      paddingTop : 46,
    },
    mainInfo : {
      flexDirection : 'row',
      justifyContent : 'space-between'
    },
    nameInfo :{
      color :'black',
    },
    locationInfo : {
      color : 'black',
      fontWeight : 'bold',
      fontSize : 15,
      marginTop : 5,
    },
    advertisementContainer : {
        backgroundColor : '#091B6F',
        marginTop : 30,
        width : '99%',
        height : 140,
        borderRadius : 8,
        paddingTop : 24,
        paddingLeft : 24,
        flexDirection : 'row'
    },
    advertisementText : {
        color : 'white',
        fontSize : 16
    },
    sewaButton : {
        backgroundColor : '#5CB85F',
        height : 28,
        width : 109,
        marginTop : 10,
        justifyContent : 'center',
        alignItems : 'center' 
    },
    sewaText : {
        color : 'white',
        fontFamily : 'Helvetica',
        fontStyle : 'normal',
        fontWeight : 'bold'
    },
    carImage : {
        width : 178,
        height : 103, 
        alignSelf : 'flex-end',
    },
    scrollHomeView : {
        paddingTop : 20,
        marginLeft : 16,
        marginRight : 16,
        paddingBottom : 100,
        marginTop : 10
    },
    menuHomeView : {
        marginTop : 110,
    },
    homeMenuList :{
        justifyContent :'space-around',
        flexDirection : 'row'
    },
    menuSewaMobil : {
        justifyContent : 'center',
        alignItems: 'center'
    },
    textMenuHome : {
        color  :'black'
    },
    daftarMobilText : {
      marginTop : 24,
      marginLeft : 16,
      color : 'black',
      fontFamily : 'Helvetica',
      fontStyle : 'normal',
      fontWeight : 'bold',
      fontSize : 14,
      lineHeight : 20,
    },
    carListContainer :{
      flexDirection : 'row',
      paddingTop : 24,  
      paddingLeft : 24,
      shadowColor: 'green',
      borderWidth : 0.1,
      borderColor : 'white',
      borderTopRightRadius : 10,
      borderBottomRightRadius : 10,
      borderTopLeftRadius : 10,
      borderBottomLeftRadius :10,
      shadowOffset: {width: -2, height: 2},
      shadowOpacity: 0.1,
      // add shadows for Android only
      // No options for shadow color, shadow offset, shadow opacity like iOS
      elevation: 3,
      width : '100%',
      height : 98,
      marginBottom : 20
    },
    carListImage : {
      width : 40,
      height : 24
    },
    carListInfo : {
      marginLeft : 16
    },
    carListText :{
      fontFamily : 'Helvetica',
      fontStyle : 'normal',
      fontWeight : 'bold',
      fontSize : 14,
      lineHeight : 20,
      color : 'black'
    },
    priceRentCar : {
      fontFamily : 'Helvetica',
      fontStyle : 'normal',
      fontWeight : 'normal',
      fontSize : 14,
      lineHeight : 20,
      color : '#5CB85F'
    },
});