import React from 'react';
import type {Node} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Button,
  TouchableOpacity,
  Image
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

export default function Account (){
    return(
        <View style={styles.mainContainer}>
          <Text style={styles.accountText}>Akun</Text>
          <View style={styles.accountContainer}>
            <Image 
              style={styles.imageStyle}
              source={require('../asset/icon/accountPict.png')}
            />
            <View style={styles.accountTextContainer}>
              <Text style={styles.textAccount}>
                Upss kamu belum memilki akun. Mulai buat akun{'\n'}
                agar transaksi di BCR lebih mudah
              </Text>
              <TouchableOpacity style={styles.registerButton}>
                <Text style={styles.registerText}>Register</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
    );
}

const styles = StyleSheet.create({
  mainContainer :{
    flex : 1,
    height : '100%'
  },
  accountText : {
    paddingTop : 18,
    paddingLeft : 20,
    paddingBottom : 18,
    marginTop : 10,
    fontFamily : 'Helvetica',
    fontStyle: 'normal',
    fontWeight : 'bold',
    fontSize : 14,
    lineHeight: 20,
    color : 'black'
  },
  accountContainer : {
    justifyContent : 'center',
    alignItems : 'center',
    paddingTop : 100
  },
  imageStyle : {
    height : 192,
    width : 312
  },
  accountTextContainer : {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop : 20
  },
  textAccount : {
    textAlign : 'center',
    color : 'black',
    fontWeight : '300',
    fontSize : 14,
    lineHeight : 20
  },
  registerButton : {
    height : 36,
    width : 81,
    backgroundColor : '#5CB85F',
    paddingTop : 8,
    paddingBottom : 8,
    paddingLeft : 12,
    paddingRight : 12,
    marginTop : 20
  },
  registerText : {
    color : 'white',
    fontFamily : 'Helvetica',
    fontStyle : 'normal',
    fontWeight : 'bold',
    fontSize : 14,
    lineHeight : 20
  }
});